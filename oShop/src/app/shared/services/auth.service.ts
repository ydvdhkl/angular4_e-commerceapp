import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { ActivatedRoute } from '@angular/router';
import { AppUser } from 'shared/model/app-user';
import { UserService } from 'shared/services/user.service';
import { mergeMap} from 'rxjs/operators';

@Injectable()
export class AuthService {

  user$: Observable<firebase.User>;

  constructor( private afAuth: AngularFireAuth, private route: ActivatedRoute, private userService: UserService ) {
    this.user$ = afAuth.authState;
   }

  login() {
    // tslint:disable-next-line:prefer-const
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  get appUser$(): Observable<AppUser> {
    return this.user$.pipe(
    mergeMap(user => {

      if (user) { return this.userService.get(user.uid); }
      return Observable.of(null);
    }));
  }
}
